//
// SQMain: This is where the module is loaded/unloaded and initialised.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once
#include <string.h>
#include <stdio.h>
#include "SQImports.h"

#ifndef WIN32
	#define sprintf_s snprintf
#endif

// Squirrel
extern "C"
{
	#include "squirrel.h"
}