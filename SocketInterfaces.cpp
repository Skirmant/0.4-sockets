#include "SocketInterfaces.h"
#include "raknet-mini/RakSleep.h"
#include "raknet-mini/TCPInterface.h"
#include "SQModule.h"
#include "sqrat/sqrat.h"

using namespace RakNet;
extern HSQAPI sq;
extern HSQUIRRELVM v;

CSquirrelSockets::CSquirrelSockets( unsigned char uc )
{
	m_ucID = uc;

	m_bStarted = false;
	m_bServer = false;
	m_pInterface = new RakNet::TCPInterface();
	m_pConnMgr = new CSquirrelSocketConnectionsMgr();
	m_sysAddr = UNASSIGNED_SYSTEM_ADDRESS;

	*m_szFunction = 0;
	*m_szNewConnFunc = 0;
	*m_szLostConnFunc = 0;
}

CSquirrelSockets::CSquirrelSockets(void)
{
	m_ucID = 255;
	m_bStarted = false;
	m_bServer = false;
	m_pInterface = NULL;
	m_pConnMgr = NULL;
	m_sysAddr = UNASSIGNED_SYSTEM_ADDRESS;

	*m_szFunction = 0;
	*m_szNewConnFunc = 0;
	*m_szLostConnFunc = 0;
}

CSquirrelSockets::~CSquirrelSockets(void)
{
	Stop();

	if ( m_pInterface )
	{
		delete m_pInterface;
		m_pInterface = NULL;
	}

	if ( m_pConnMgr )
	{
		delete m_pConnMgr;
		m_pConnMgr = NULL;
	}
}

bool CSquirrelSockets::Connect( const char* szHost, unsigned short usPort )
{
	if ( m_pInterface )
	{
		if ( !m_bStarted )
		{
			m_pInterface->Start( 0, 0 );

			RakSleep( 5 );

			m_sysAddr = m_pInterface->Connect( szHost, usPort, false );

			if ( m_sysAddr != UNASSIGNED_SYSTEM_ADDRESS ) 
			{
				m_bServer = false;

				return true;
			}
		}
	}

	return false;
}

bool CSquirrelSockets::Start( unsigned short usPort, unsigned short usMaxConns )
{
	if ( m_pInterface )
	{
		if ( !m_bStarted )
		{
			if ( m_pInterface->Start( usPort, usMaxConns ) )
			{
				m_bStarted = true;
				m_bServer = true;

				return true;
			}
		}
	}

	return false;
}

void CSquirrelSockets::Stop( void )
{
	if ( m_pInterface )
	{
		m_pInterface->Stop();

		m_bStarted = false;
		m_bServer = false;
	}
}

void CSquirrelSockets::Send( char* sz, unsigned char ucConn )
{
	if ( m_pInterface )
	{
		if ( m_bServer )
		{
			if ( ucConn == 255 )
			{
				unsigned char uc = 0, uc1 = 0;
				while ( ( uc < 128 ) && ( uc1 < m_pConnMgr->Count() ) )
				{
					SystemAddress pID = m_pConnMgr->Find( uc );
					if ( pID != UNASSIGNED_SYSTEM_ADDRESS )
					{
						m_pInterface->Send( sz, (unsigned int)strlen( sz ), pID, false );
						uc1++;
					}
					uc++;
				}
			}
			else
			{
				SystemAddress pID = m_pConnMgr->Find( ucConn );
				if ( pID != UNASSIGNED_SYSTEM_ADDRESS ) m_pInterface->Send( sz, (unsigned int)strlen( sz ), pID, false );
			}
		}
		else
		{
			if ( m_sysAddr != UNASSIGNED_SYSTEM_ADDRESS ) m_pInterface->Send( sz, (unsigned int)strlen( sz ), m_sysAddr, false );
		}
	}
}

void CSquirrelSockets::CloseConnection( unsigned char ucConn )
{
	if ( m_pInterface )
	{
		if ( m_bServer )
		{
			SystemAddress pID = m_pConnMgr->Find( ucConn );

			if ( pID != UNASSIGNED_SYSTEM_ADDRESS ) m_pInterface->CloseConnection( pID );
		}
	}
}

void CSquirrelSockets::Process( void )
{
	if ( m_pInterface )
	{
		if ( *m_szFunction )
		{
			Packet *p = m_pInterface->Receive();

			SystemAddress player = m_pInterface->HasCompletedConnectionAttempt();
			if ( ( player != UNASSIGNED_SYSTEM_ADDRESS )  )
			{
				m_bStarted = true;
				m_sysAddr = player;

				if ( *m_szNewConnFunc )
				{
					if ( sq && v )
					{
						Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szNewConnFunc);
						if (!f.IsNull())
							f();

						f.Release();
					}
				}
			}

			player = m_pInterface->HasFailedConnectionAttempt();
			if ( ( player != UNASSIGNED_SYSTEM_ADDRESS )  )
			{
				m_bStarted = false;
				 
				if ( *m_szLostConnFunc )
				{
					if ( sq && v )
					{
						Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szLostConnFunc);
						if (!f.IsNull())
							f();

						f.Release();
					}
				}
			}

			player = m_pInterface->HasNewIncomingConnection();
			if ( player != UNASSIGNED_SYSTEM_ADDRESS ) 
			{
				unsigned char uc = m_pConnMgr->New( player );
				if ( *m_szNewConnFunc )
				{
					if ( sq && v )
					{
						Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szNewConnFunc);
						if (!f.IsNull())
							f(uc, (char *)player.ToString(false), player.GetPort());

						f.Release();
					}
				}
			}

			player = m_pInterface->HasLostConnection();
			if ( player != UNASSIGNED_SYSTEM_ADDRESS ) 
			{
				unsigned char uc = m_pConnMgr->Find( player );
				if ( uc != 255 )
				{
					if ( *m_szNewConnFunc )
					{
						if ( sq && v )
						{
							Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szLostConnFunc);
							if (!f.IsNull())
								f(uc, (char*)player.ToString(false), player.GetPort());

							f.Release();
						}
					}

					m_pConnMgr->Remove( player );
				}
			}

			if ( p )
			{
				if ( sq && v )
				{
					if ( m_bServer )
					{
						unsigned char uc = m_pConnMgr->Find(p->systemAddress);
						Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szFunction);
						if (!f.IsNull())
							f(uc, (char*)p->data);

						f.Release();
					}
					else
					{
						Sqrat::Function f = Sqrat::RootTable(v).GetFunction(m_szFunction);
						if (!f.IsNull())
							f((char*)p->data);

						f.Release();
					}
				}

				m_pInterface->DeallocatePacket( p );
			}
		}
	}
}

// The Manager
unsigned char CSquirrelSocketManager::m_ucSockets = 0;
CSquirrelSockets* CSquirrelSocketManager::m_Sockets[ MAX_SOCKETS ] = { 0 };

CSquirrelSockets* CSquirrelSocketManager::New( void )
{
	unsigned int uiID = FindFreeID();
	if ( uiID < MAX_SOCKETS )
	{
		CSquirrelSockets* pTimer = new CSquirrelSockets( uiID );
		if ( pTimer )
		{
			m_Sockets[ uiID ] = pTimer;
			m_ucSockets++;
			return pTimer;
		}
	}
	
	return 0;
}

CSquirrelSockets* CSquirrelSocketManager::New( unsigned char ucID )
{
	if ( ucID < MAX_SOCKETS )
	{
		CSquirrelSockets* pTimer = new CSquirrelSockets( ucID );
		if ( pTimer )
		{
			m_Sockets[ ucID ] = pTimer;
			m_ucSockets++;
			return pTimer;
		}
	}
	
	return 0;
}

CSquirrelSockets* CSquirrelSocketManager::Find( unsigned char uc )
{
	if ( uc < MAX_SOCKETS ) return m_Sockets[ uc ];
	return 0;
}

bool CSquirrelSocketManager::Remove( CSquirrelSockets* p )
{
	if ( p )
	{
		if ( p->GetID() < MAX_SOCKETS )
		{
			m_Sockets[ p->GetID() ] = 0;
			m_ucSockets--;

			delete p;

			return true;
		}
	}

	return false;
}

bool CSquirrelSocketManager::Remove( unsigned char uc )
{
	CSquirrelSockets* p = Find( uc );
	if ( p )
	{
		m_Sockets[ uc ] = 0;
		m_ucSockets--;
		delete p;

		return true;
	}

	return false;
}

void CSquirrelSocketManager::RemoveAll( void )
{
	unsigned int uc = 0, uc1 = 0;
	while ( ( uc < MAX_SOCKETS ) && ( uc1 < m_ucSockets ) )
	{
		if ( m_Sockets[ uc ] )
		{
			delete m_Sockets[ uc ];

			m_Sockets[ uc ] = 0;

			uc1++;
		}
		uc++;
	}

	m_ucSockets = 0;
}

unsigned char CSquirrelSocketManager::FindFreeID( void )
{
	for ( unsigned char uc = 0; uc < MAX_SOCKETS; uc++ )
	{
		if ( !m_Sockets[ uc ] ) return uc;
	}

	return MAX_SOCKETS;
}

void CSquirrelSocketManager::ProcessSockets( void )
{
	unsigned char uc = 0, uc1 = 0;
	while ( ( uc < MAX_SOCKETS ) && ( uc1 < m_ucSockets ) )
	{
		if ( m_Sockets[ uc ] )
		{
			m_Sockets[ uc ]->Process();

			uc1++;
		}
		uc++;
	}
}

// The Connection Manager
CSquirrelSocketConnectionsMgr::CSquirrelSocketConnectionsMgr()
{
	for ( unsigned int uc = 0; uc < 128; uc++ )
	{
		m_Connections[ uc ] = UNASSIGNED_SYSTEM_ADDRESS;
	}
}

unsigned char CSquirrelSocketConnectionsMgr::New( SystemAddress sysAddr )
{
	unsigned char ucID = FindFreeID();
	if ( ucID < 128 )
	{
		m_Connections[ ucID ] = sysAddr;
		m_ucConnections++;
		return ucID;
	}
	
	return 255;
}

unsigned char CSquirrelSocketConnectionsMgr::Find( SystemAddress sysAddr )
{
	for ( unsigned char uc = 0; uc < 128; uc++ )
	{
		if ( m_Connections[ uc ] == sysAddr ) return uc;
	}

	return 255;
}

SystemAddress CSquirrelSocketConnectionsMgr::Find( unsigned char uc )
{
	if ( uc < 128 ) return m_Connections[ uc ];
	return UNASSIGNED_SYSTEM_ADDRESS;
}

bool CSquirrelSocketConnectionsMgr::Remove( SystemAddress sysAddr )
{
	if ( sysAddr != UNASSIGNED_SYSTEM_ADDRESS )
	{
		unsigned char uc = Find( sysAddr );
		if ( uc != 255 )
		{
			m_Connections[ uc ] = UNASSIGNED_SYSTEM_ADDRESS;
			m_ucConnections--;

			return true;
		}
	}

	return false;
}

bool CSquirrelSocketConnectionsMgr::Remove( unsigned char uc )
{
	if ( uc != 255 )
	{
		m_Connections[ uc ] = UNASSIGNED_SYSTEM_ADDRESS;
		m_ucConnections--;

		return true;
	}

	return false;
}

void CSquirrelSocketConnectionsMgr::RemoveAll( void )
{
	unsigned int uc = 0, uc1 = 0;
	while ( ( uc < 128 ) && ( uc1 < m_ucConnections ) )
	{
		if ( m_Connections[ uc ] != UNASSIGNED_SYSTEM_ADDRESS )
		{
			m_Connections[ uc ] = UNASSIGNED_SYSTEM_ADDRESS;

			uc1++;
		}
		uc++;
	}

	m_ucConnections = 0;
}

unsigned char CSquirrelSocketConnectionsMgr::FindFreeID( void )
{
	for ( unsigned char uc = 0; uc < 128; uc++ )
	{
		if ( m_Connections[ uc ] == UNASSIGNED_SYSTEM_ADDRESS ) return uc;
	}

	return 128;
}
