#include "SocketInterfaces.h"
#include "SQMain.h"
#include "SQFuncs.h"
#include "plugin.h"

#ifdef WIN32
	#define EXPORT __declspec(dllexport)
	#include <Windows.h>
	#define _WIN32_LEAN_AND_MEAN
#else
	#define EXPORT
#endif

void OutputMessage( const char * msg )
{
	#ifdef WIN32
		HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );

		CONSOLE_SCREEN_BUFFER_INFO csbBefore;
		GetConsoleScreenBufferInfo( hstdout, &csbBefore );
		SetConsoleTextAttribute( hstdout, FOREGROUND_GREEN );
		printf( "[MODULE]  " );
		
		SetConsoleTextAttribute( hstdout, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY );
		printf( "%s\n", msg );

		SetConsoleTextAttribute( hstdout, csbBefore.wAttributes );
	#else
		printf( "%c[0;32m[MODULE]%c[0;37m %s\n", 27, 27, msg );
	#endif
}

void OutputDebug( const char * msg )
{
	#ifdef _DEBUG
		OutputMessage( msg );
	#endif
}

PluginFuncs * gFuncs;
HSQUIRRELVM v;
HSQAPI sq;

void OnSquirrelScriptLoad()
{
	// See if we have any imports from Squirrel
	unsigned int size;
	int     sqId      = gFuncs->FindPlugin( "SQHost2" );
	void ** sqExports = gFuncs->GetPluginExports( sqId, &size );

	// We do!
	if( sqExports != NULL && size > 0 )
	{
		// Cast to a SquirrelImports structure
		SquirrelImports ** sqDerefFuncs = (SquirrelImports **)sqExports;
		
		// Now let's change that to a SquirrelImports pointer
		SquirrelImports * sqFuncs       = (SquirrelImports *)(*sqDerefFuncs);
		
		// Now we get the virtual machine
		if( sqFuncs )
		{
			// Get a pointer to the VM and API
			v = *(sqFuncs->GetSquirrelVM());
			sq = *(sqFuncs->GetSquirrelAPI());

			// Register functions
			RegisterFuncs( v );
		}
	}
	else
		OutputMessage( "Failed to attach to SQHost2." );
}

int OnInternalCommand(unsigned int uCmdType, const char* pszText)
{
	switch( uCmdType )
	{
		case 0x7D6E22D8:
			OnSquirrelScriptLoad();
			break;

		default:
			break;
	}

	return 1;
}

int OnInitServer()
{
	printf( "\n" );
	OutputMessage( "Loaded sq_sockets for VC:MP by the LU Dev Team (ported by Stormeus)." );

	return 1;
}

void OnFrame(float fElapsedTime) { CSquirrelSocketManager::ProcessSockets(); }
void OnShutdownServer() { CSquirrelSocketManager::RemoveAll(); }

extern "C" EXPORT unsigned int VcmpPluginInit( PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info )
{
	// Set our plugin information
	info->uPluginVer = 0x1000; // 1.0.00
	memcpy(info->szName, "sq_sockets", sizeof(info->szName));

	// Store functions for later use
	gFuncs = functions;

	// Store callback
	callbacks->OnInitServer = OnInitServer;
	callbacks->OnFrame = OnFrame;
	callbacks->OnShutdownServer = OnShutdownServer;
	callbacks->OnInternalCommand = OnInternalCommand;

	// Done!
	return 1;
}