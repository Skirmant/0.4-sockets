#include "SQFuncs.h"
#include "SocketInterfaces.h"
#include "sqrat/sqrat.h"
#include <stdio.h>

extern HSQAPI sq;
CSquirrelSockets * NewSocket(SQChar* pszDataFunc)
{
	CSquirrelSockets * pSocket = CSquirrelSocketManager::New();
	if (pSocket == NULL) {
		return NULL;
	}

	pSocket->SetFunction(pszDataFunc);
	return pSocket;
}

void RegisterFuncs( HSQUIRRELVM v )
{
	Sqrat::RootTable(v).Func("NewSocket", NewSocket, 2, "ts");
	Sqrat::ImprovedClass<CSquirrelSockets> socketClass(v);
	socketClass
		.Func("Connect", &CSquirrelSockets::Connect, 3, "xsi")
		.Func("Disconnect", &CSquirrelSockets::CloseConnection, 2, "xi")
		.Func("Send", &CSquirrelSockets::SqSend, 2, "xs")
		.Func("SendClient", &CSquirrelSockets::SqSendClient, 3, "xsi")
		.Func("SetLostConnFunc", &CSquirrelSockets::SetLostConnFunction, 2, "xs")
		.Func("SetNewConnFunc", &CSquirrelSockets::SetNewConnFunction, 2, "xs")
		.Func("Start", &CSquirrelSockets::Start, 3, "xii")
		.Func("Stop", &CSquirrelSockets::Stop, 1, "x")

		.Func("Delete", &CSquirrelSockets::DeleteMe, 1, "x");
}
